from texas_holdem.player import Player
from texas_holdem.rulesets.ruleset import TexasHoldemRuleSet


class TestTexasHoldemRuleSet:

    def test_should_be_able_to_rank_a_players_hand_with_the_default_ruleset_for_no_rank(self):
        # Given
        test_hand = "1c Ah Ks 2d 9d 3c 6d"
        ruleset = TexasHoldemRuleSet()
        test_name = "test_player"
        player = Player(name=test_name, hand=test_hand)

        expected_rank = ruleset.ranking.NOTHING

        # When

        rank = ruleset.rank_hand(player)

        # Then
        assert rank == expected_rank

    def test_should_be_able_to_rank_a_players_hand_with_a_pair(self):
        # Given
        test_hand = "9c Ah Ks 2d 9d 3c 6d"
        ruleset = TexasHoldemRuleSet()
        test_name = "test_player"
        player = Player(name=test_name, hand=test_hand)

        expected_rank = ruleset.ranking.ONE_PAIR

        # When

        rank = ruleset.rank_hand(player)

        # Then
        assert rank == expected_rank

    def test_should_be_able_to_rank_a_players_hand_with_a_two_pair(self):
        # Given
        test_hand = "9c Ah As 2d 9d 3c 6d"
        ruleset = TexasHoldemRuleSet()
        test_name = "test_player"
        player = Player(name=test_name, hand=test_hand)

        expected_rank = ruleset.ranking.TWO_PAIR

        # When

        rank = ruleset.rank_hand(player)

        # Then
        assert rank == expected_rank

    def test_should_be_able_to_rank_a_players_hand_with_a_three_of_a_kind(self):
        # Given
        test_hand = "9c Ah As Ad 1d 3c 6d"
        ruleset = TexasHoldemRuleSet()
        test_name = "test_player"
        player = Player(name=test_name, hand=test_hand)

        expected_rank = ruleset.ranking.THREE_OF_A_KIND

        # When

        rank = ruleset.rank_hand(player)

        # Then
        assert rank == expected_rank

    def test_should_have_a_ranking_of_player_hands(self):
        # Given
        ruleset = TexasHoldemRuleSet()

        # When

        result = ruleset.ranking

        # Then
        assert result is not None

    def test_should_have_a_ranking_of_player_hands_that_it_is_ordered(self):
        # Given
        ruleset = TexasHoldemRuleSet()

        # When

        rank1 = ruleset.ranking.ONE_PAIR
        rank2 = ruleset.ranking.TWO_PAIR

        # Then
        assert rank1 < rank2
