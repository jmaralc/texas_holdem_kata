from texas_holdem.player import Player


class TestPlayer:

    def test_should_have_a_name(self):
        # Given
        test_name = "test_player"

        # When
        player = Player(name=test_name)

        # Then
        assert player.name == test_name

    def test_should_start_with_an_initial_empty_hand(self):
        # Given
        test_name = "test_player"

        # When
        player = Player(name=test_name)

        # Then
        assert not player.hand

    def test_should_start_with_a_hand_as_long_as_the_amount_of_cards(self):
        # Given
        test_hand = "Kc 9s Ks Kd 9d 3c 6d"
        test_name = "test_player"

        # When
        player = Player(name=test_name, hand=test_hand)

        # Then
        assert len(player.hand) == 7

    def test_should_be_able_to_show_their_hand(self, capsys):
        # Given
        test_hand = "Kc 9s Ks Kd 9d 3c 6d"
        test_name = "test_player"

        # When
        player = Player(name=test_name, hand=test_hand)
        player.show_hand()

        # Then
        captured = capsys.readouterr()
        assert captured.out == f"{test_hand}\n"
