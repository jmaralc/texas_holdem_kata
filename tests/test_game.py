import pytest
from texas_holdem.game import Game
from texas_holdem.player import Player
from texas_holdem.rulesets.ruleset import RuleSet, TexasHoldemRuleSet


class TestGame:

    def test_should_be_created_with_a_number_of_players_and_type_of_game(self):
        # Given
        test_game_name = "poker"
        test_number_of_players = 4

        # When

        game = Game(name=test_game_name, n_players=test_number_of_players)

        # Then
        assert (
            len(game.players) == test_number_of_players and
            game.name == test_game_name
        )

    def test_should_be_able_to_add_a_player_if_enough_room_in_the_game(self):
        # Given
        test_game_name = "poker"
        test_number_of_players = 4

        test_hand = "9c Ah Ks Kd 9d 3c 6d"
        test_name = "test_player"
        player = Player(name=test_name, hand=test_hand)

        # When

        game = Game(name=test_game_name, n_players=test_number_of_players)
        game.add_player(player)

        # Then
        assert (
            game.players[0] == player and
            not all(game.players[1:])
        )

    def test_should_print_a_warning_if_the_game_is_already_full_of_players(self, capsys):
        # Given
        test_game_name = "poker"
        test_number_of_players = 2

        test_hand = "9c Ah Ks Kd 9d 3c 6d"
        test_name = "test_player"
        player0 = Player(name=f"{test_name}0", hand=test_hand)
        player1 = Player(name=f"{test_name}1", hand=test_hand)
        player2 = Player(name=f"{test_name}2", hand=test_hand)

        # When

        game = Game(name=test_game_name, n_players=test_number_of_players)
        game.add_player(player0)
        game.add_player(player1)
        game.add_player(player2)

        # Then
        captured = capsys.readouterr()
        assert captured.out == "The game is full. No more players allowed\n"

    def test_should_have_a_valid_ruleset_default_based_on_the_name_of_the_game(self):
        # Given
        test_game_name = "default"
        test_number_of_players = 4

        # When

        game = Game(name=test_game_name, n_players=test_number_of_players)

        # Then
        assert isinstance(game.ruleset, RuleSet)

    def test_should_have_a_valid_ruleset_for_texas_holdem(self):
        # Given
        test_game_name = "texas_holdem"
        test_number_of_players = 4

        # When

        game = Game(name=test_game_name, n_players=test_number_of_players)

        # Then
        assert isinstance(game.ruleset, TexasHoldemRuleSet)

    def test_should_be_able_to_check_the_winner_based_on_a_ruleset_and_players_cards(self, capsys):
        # Given
        test_game_name = "texas_holdem"
        test_number_of_players = 2
        test_name = "test_player"

        player0_hand = "9c 9s Ks Kd 1d 3c 6d"
        player0 = Player(name=f"{test_name}0", hand=player0_hand)
        player1_hand = "9c Kh Ks Kd 1d 3c 6d"
        player1 = Player(name=f"{test_name}1", hand=player1_hand)

        game = Game(name=test_game_name, n_players=test_number_of_players)
        game.add_player(player0)
        game.add_player(player1)

        expected_result = """9c 9s Ks Kd 1d 3c 6d Two pair\n9c Kh Ks Kd 1d 3c 6d Three of a kind (winner)\n"""

        # When
        game.check_winner()

        # Then
        captured = capsys.readouterr()
        assert captured.out == expected_result
