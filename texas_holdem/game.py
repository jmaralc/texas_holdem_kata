from texas_holdem.player import Player
from typing import List, Optional
from texas_holdem.rulesets.ruleset import RuleSetFactory
from enum import Enum


class Game:

    def __init__(self, name: str, n_players: int):
        self.name = name
        self.players: List[Optional[Player]] = [None] * n_players
        self.ruleset = RuleSetFactory().set_game(game_name=name)

    def add_player(self, player: Player) -> None:
        try:
            first_empty_position = self.players.index(None)
            self.players[first_empty_position] = player
        except ValueError:
            print("The game is full. No more players allowed")

    def _format_rank(self, rank: Enum):
        return rank.name.lower().replace("_", " ").capitalize()

    def check_winner(self) -> None:

        players_ranks = [self.ruleset.rank_hand(player) for player in self.players]
        winner_player = players_ranks.index(max(players_ranks))

        for player_index, player in enumerate(self.players):
            player_hand = " ".join(player.hand)
            formatted_rank = self._format_rank(players_ranks[player_index])
            if player_index == winner_player:
                print(f"{player_hand} {formatted_rank} (winner)")
            else:
                print(f"{player_hand} {formatted_rank}")
