from typing import Optional, Dict, List


class Player:

    def __init__(self, name: str, hand: Optional[str] = None) -> None:
        self.name = name

        if hand is None:
            self.hand = []
        else:
            self.hand = hand.split(" ")

    def show_hand(self) -> None:
        print(" ".join(self.hand))
