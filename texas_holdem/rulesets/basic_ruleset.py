from abc import ABC, abstractmethod
from texas_holdem.player import Player
from enum import Enum, auto


class RuleSet(ABC):

    @abstractmethod
    def rank_hand(self, player: Player) -> Enum:
        pass


class EveryBodyWinRanks(Enum):
    WINNER = auto()


class EveryBodyWinRuleSet(RuleSet):

    ranking = EveryBodyWinRanks

    def rank_hand(self, player: Player) -> Enum:
        return self.ranking.WINNER
