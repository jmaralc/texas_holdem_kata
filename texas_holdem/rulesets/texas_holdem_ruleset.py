from typing import List, Dict
from texas_holdem.rulesets.ruleset import RuleSet
from texas_holdem.player import Player
from enum import Enum


class OrderedEnum(Enum):
    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


class TexasHoldemRanking(OrderedEnum):
    NOTHING = 0
    ONE_PAIR = 1
    TWO_PAIR = 2
    THREE_OF_A_KIND = 3


class TexasHoldemRuleSet(RuleSet):

    ranking = TexasHoldemRanking

    def _check_for_one_pair(self, hand) -> bool:
        hand_faces: List[str] = [card[0] for card in hand]
        faces_counter: Dict[str, int] = {face: 0 for face in hand_faces}

        for face in hand_faces:
            faces_counter[face] += 1

        pairs = 0
        for count in faces_counter.values():
            if count == 2:
                pairs += 1

        if pairs == 1:
            return True
        return False

    def _check_for_two_pair(self, hand):
        hand_faces: List[str] = [card[0] for card in hand]
        faces_counter: Dict[str, int] = {face: 0 for face in hand_faces}

        for face in hand_faces:
            faces_counter[face] += 1

        pairs = 0
        for count in faces_counter.values():
            if count == 2:
                pairs += 1

        if pairs == 2:
            return True
        return False

    def _check_for_three_of_a_kind(self, hand):
        hand_faces: List[str] = [card[0] for card in hand]
        faces_counter: Dict[str, int] = {face: 0 for face in hand_faces}

        for face in hand_faces:
            faces_counter[face] += 1

        thirds = 0
        for count in faces_counter.values():
            if count == 3:
                thirds += 1

        if thirds == 1:
            return True
        return False

    def rank_hand(self, player: Player) -> Enum:
        if self._check_for_one_pair(player.hand):
            return self.ranking.ONE_PAIR
        elif self._check_for_two_pair(player.hand):
            return self.ranking.TWO_PAIR
        elif self._check_for_three_of_a_kind(player.hand):
            return self.ranking.THREE_OF_A_KIND
        else:
            return self.ranking.NOTHING
