from typing import Dict
from texas_holdem.rulesets.basic_ruleset import RuleSet, EveryBodyWinRuleSet
from texas_holdem.rulesets.texas_holdem_ruleset import TexasHoldemRuleSet


class RuleSetFactory:

    games: Dict[str, RuleSet] = {
        "texas_holdem": TexasHoldemRuleSet
    }

    def set_game(self, game_name: str) -> RuleSet:
        selected_ruleset: RuleSet = self.games.get(game_name, EveryBodyWinRuleSet)
        return selected_ruleset()
